<?php
class rObject{

    private $id;
    private $name;
    private $photo;
    private $address;
    private $price;
    private $description;
    private $state;

//    public function __construct($post){
//
//        $this->id = $post['id'];
//        $this->name = $post['name'];
//        $this->photo = $post['photo'];
//        $this->address = $post['address'];
//        $this->price = $post['price'];
//        $this->description = $post['description'];
//        $this->state = $post['state'];
//    }

    public function getId(){

        return $this->id;
    }

    public function getData(){

        return array(
            'id' =>$this->id,
            'name' => $this->name,
            'photo' => $this->photo,
            'address' => $this->address,
            'price' => $this->price,
            'description' => $this->description,
            'state' => $this->state
        );
    }
}