<?php

$dsn = "mysql:host=127.0.0.1;dbname=test_quest;charset=utf8";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_WARNING,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS];

try {
    $dbh = new PDO($dsn, 'admin', 'secret', $opt);
}
catch (PDOException $e){

    echo $e->getMessage(), exit();
}