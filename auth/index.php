<?php

include '../lib/object.php';
include '../lib/pdo.php';
include '../lib/session.php';

if($_POST['type'] == 'login'){

    $sth = $dbh->prepare("select id from users where (username=:username and password=:password)");
    $sth->execute(array('username' => $_POST['username'], 'password' => $_POST['password']));

    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $userId = $sth->fetch();

    $id = 0;

    if(isset($userId["id"])){

        $id = $userId['id'];
    }

    if($id > 0){

        $_SESSION['userid'] = $id;
        echo json_encode(['auth'=> 'ok']);
    }
    else{

        echo json_encode(['auth'=> 'fail']);
    }
}
if($_POST['type'] == 'logout'){

    if(isset($_SESSION['userid'])){

        unset($_SESSION['userid']);
        echo json_encode(['unauth'=> 'ok']);
    }
}
if($_POST['type'] == 'status'){

    if(isset($_SESSION['userid'])){

        echo json_encode(['auth'=> 'ok']);
    }
    else{

        echo json_encode(['auth'=> 'fail']);
    }
}
