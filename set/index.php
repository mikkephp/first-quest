<?php

include '../lib/object.php';
include '../lib/pdo.php';
include '../lib/session.php';

if(!isset($_SESSION['userid'])){

    echo json_encode(['auth' => false]);
    exit(0);
}

if($_POST['id'] != 0){

    $sth = $dbh->prepare("update objects set name=:name, image=:image, price=:price, description=:description, state=:state, address=:address where id=:id");

    try {

        $sth->execute(array(
            'id' => $_POST['id'],
            'name' => $_POST['name'],
            'image' => $_POST['image'],
            'price' => $_POST['price'],
            'description' => $_POST['description'],
            'state' => $_POST['state'],
            'address' => $_POST['address']));
    }
    catch (PDOException $e){

        echo $e->getMessage(), exit();
    }

    echo json_encode(['mod' => true]);
}
else{

    $sth = $dbh->prepare("insert into objects (name, image, price, description, state, address) values (:name, :image, :price, :description, :state, :address)");

    try {

        $sth->execute(array(
            'name' => $_POST['name'],
            'image' => $_POST['image'],
            'price' => $_POST['price'],
            'description' => $_POST['description'],
            'state' => $_POST['state'],
            'address' => $_POST['address']));
    }
    catch (PDOException $e){

        echo $e->getMessage(), exit();
    }

    echo json_encode(['add' => true]);
}
