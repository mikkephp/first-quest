<?php

include '../lib/object.php';
include '../lib/pdo.php';
include '../lib/session.php';

$result['auth'] = false;

$order = '';

if(isset($_POST['sortAsc'])){

    $order = "price, ";
}
if(isset($_POST['sortDesc'])){

    $order = "price desc, ";
}

if(isset($_SESSION['userid'])){

    $result['auth'] = true;
    $query = 'select id, name, address, price, description, state from objects order by state desc, ' . $order . 'id desc ';
}
else{

    $query = "select id, name, address, price, description, state from objects where state = 1 order by " . $order . "id desc ";
}


$sth = $dbh->query($query);
$sth->setFetchMode(PDO::FETCH_CLASS, 'rObject' );


while($object = $sth->fetch()){

    $result['objects'][] = $object->getData();
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);