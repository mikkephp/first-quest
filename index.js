$(document).ready(function() {

    // получение списка объектов с сервера
    function getRecords(sort = '', bldNum = '') {

        var post = {};

        if(sort == 'asc'){

            post = {'sortAsc':'1'};
        }
        if(sort == 'desc'){

            post = {'sortDesc':'1'};
        }


        $.post('/get/',
            post,
            function(data) {

            $('#rObjects').html('');
            var rObjectState;

            data.objects.forEach(function(item, i) {


               if(bldNum != ''){

                   var currentBldNum = item.address.split('-')[0].split(' ');
                   var currentBldNum = currentBldNum[currentBldNum.length - 1];

                   if(currentBldNum != bldNum){

                       return;
                   }
               }

                var editButton = '&nbsp;';

                if(data.auth) {
                    if (item.state == 1) {
                        rObjectState = '<div class="alert alert-success rObjectState" role="alert" ract="1">Активно</div>';
                    } else {
                        rObjectState = '<div class="alert alert-danger rObjectState" role="alert" ract="0">Неактивно</div>';
                    }

                    editButton = '<div class="col-12"><a href="#" class="btn btn-edit btn-secondary" id="edit-' + item.id + '">Редактировать</a>';
                }
                else{
                    rObjectState = '<div class="alert alert-success rObjectState d-none" role="alert" ract="1">Активно</div>';
                }

                var row = '       <div class="row py-3" id="record' + item.id + '">\n' +
                    '        <div class="order-2 order-md-1 col-md-5">\n' +
                    '          <img class="img-fluid d-block" src="https://static.pingendo.com/img-placeholder-tablet.svg">\n' +
                    '       </div>\n' +
                    '        <div class="px-md-3 d-flex flex-column justify-content-center col-md-7 order-1 order-md-2">\n' +
                    '          <div class="row">\n' +
                    '               <div class="col-9 rObjectName"><h1>' + item.name + '</h1></div>\n' +
                    '               <div class="col-3">' +  rObjectState + '</div>\n' +
                    '          </div>\n' +
                    '          <div class="row">\n' +
                    '             <div class="mb-3 col-3"><b>Адрес&nbsp;</b></div><div class="col-9 rObjectAddress">' + item.address + '</div>\n' +
                    '          </div>\n' +
                    '          <div class="row">\n' +
                    '              <div class="mb-3 col-3"><b>Цена&nbsp;</b></div><div class="col-9 rObjectPrice">' + item.price + '</div>\n' +
                    '          </div>\n' +
                    '          <div class="row">\n' +
                    '               <div class="mb-3 col-3"><b>Описание&nbsp;</b></div><div class="col-9 rObjectDescription">' + item.description + '</div>\n' +
                    '          </div>\n' +
                    '          <div class="row">\n' +
                    '               ' + editButton + '\n' +
                    '          </div>\n' +
                    '        </div>\n' +
                    '      </div>\n' +
                    '    </div>\n';

                $('#rObjects').append(row);
            })
        },
        'json');
    }

    // открытие окна редактирования объекта недвижмости
    $('div').click(function(e){

        // выбираем только нужный клик из всех куда угодно
        var target = e.target.id.split('-');

        if(target[0] == 'edit' && e.currentTarget.id == "rObjects"){

            var id = target[1];
            var state = $('#record' + id + ' .rObjectState')[0].getAttribute('ract');

            $('#c_form-h')[0].setAttribute('rObjectId', id);
            $('#c_form-h')[0].setAttribute('rObjectState', state);

            // заполнение формы редактирования данных объекта
            $('#inputName').val($('#record' + id + ' h1')[0].outerText);
            $('#inputAddress').val($('#record' + id + ' .rObjectAddress')[0].innerHTML);
            $('#inputPrice').val($('#record' + id + ' .rObjectPrice')[0].innerHTML);
            $('#inputDescription').val($('#record' + id + ' .rObjectDescription')[0].innerHTML);


            // устанка активности/неактивности
            if(state == 1){

                $('#inputObjectStateActive')[0].classList.remove('btn-outline-primary');
                $('#inputObjectStateInactive')[0].classList.remove('btn-danger');
                $('#inputObjectStateActive')[0].classList.add('btn-primary');
                $('#inputObjectStateInactive')[0].classList.add('btn-outline-danger');
            }
            else{

                $('#inputObjectStateActive')[0].classList.remove('btn-primary');
                $('#inputObjectStateInactive')[0].classList.remove('btn-outline-danger');
                $('#inputObjectStateActive')[0].classList.add('btn-outline-primary');
                $('#inputObjectStateInactive')[0].classList.add('btn-danger');
            }


            $('#addEditRObject').modal('show');
        }
    });

    // обработка кнопки Активно
    $('#inputObjectStateActive').click(function(){

        $('#c_form-h')[0].setAttribute('rObjectState', "1");

        $('#inputObjectStateActive')[0].classList.remove('btn-outline-primary');
        $('#inputObjectStateInactive')[0].classList.remove('btn-danger');
        $('#inputObjectStateActive')[0].classList.add('btn-primary');
        $('#inputObjectStateInactive')[0].classList.add('btn-outline-danger');
    });

    // обработка кнопки Неактивно
    $('#inputObjectStateInactive').click(function(){

        $('#c_form-h')[0].setAttribute('rObjectState', "0");

        $('#inputObjectStateActive')[0].classList.remove('btn-primary');
        $('#inputObjectStateInactive')[0].classList.remove('btn-outline-danger');
        $('#inputObjectStateActive')[0].classList.add('btn-outline-primary');
        $('#inputObjectStateInactive')[0].classList.add('btn-danger');
    });

    // открытие окна входа в систему
    $('#login').click(function () {

        $('#loginWindow').modal('show');
    });

    // выход из системы
    $('#logout').click(function () {

        $.post(
            '/auth/',
            {'type':'logout'},
            function (d) {

                // сокрытие более не нужного
                if(d['unauth'] == 'ok'){

                    $('#login')[0].classList.remove('d-none');
                    $('#logout')[0].classList.add('d-none');
                    $('#buttonAdd')[0].classList.add('d-none');

                    for(i = 0; i < $('div.rObjectState').length; i++){

                        $('div.rObjectState')[i].classList.add('d-none')
                    }

                    // обновление списка
                    getRecords();
                }
            },
            'json');

    });

    // обработка кнопки вход в систему
    $('#tryLogin').click(function () {

        var data = {
            'type':'login',
            'username' : $('#inputUserName').val(),
            'password' : $('#inputUserPassword').val()};

        $.post(
            '/auth/',
            data,
            function (d) {

                if(d['auth'] == 'ok'){

                    $('#login')[0].classList.add('d-none');
                    $('#logout')[0].classList.remove('d-none');
                    $('#buttonAdd')[0].classList.remove('d-none');
                    getRecords();
                    $('#loginWindow').modal('hide');
                }
            },
            'json');
    });

    // создание нового объекта
    $('#buttonAdd').click(function () {

        $('#c_form-h')[0].setAttribute('rObjectState', "1");
        $('#addEditRObject').modal('show');
    });
    
    // проверка входа в систему
    function checkLogin(){

        $.post(
            '/auth/',
            {'type':'status'},
            function (d) {

                if(d['auth'] == 'ok'){

                    $('#login')[0].classList.add('d-none');
                    $('#logout')[0].classList.remove('d-none');
                    $('#buttonAdd')[0].classList.remove('d-none');
                    $('#loginWindow').modal('hide');
                }
            },
            'json');

    }

    // сохранение записи
    $('#saveObjectChanges').click(function () {

        // id записи, если есть
        var id = 0;
        
        if($('#c_form-h ')[0].hasAttribute('rObjectId')){

            id = $('#c_form-h')[0].getAttribute('rObjectId');
        }

        // сбор данных для отправки
        var data = {
            'id':id,
            'name':$('#inputName').val(),
            'address':$('#inputAddress').val(),
            'price':$('#inputPrice').val(),
            'description':$('#inputDescription').val(),
            'image':'-',
            'state':$('#c_form-h')[0].getAttribute('rObjectState')};

        $.post(
            '/set/',
            data,
            function(a){

                // очистка формы редактирования данных объекта
                $('#c_form-h')[0].setAttribute('rObjectId', '');
                $('#c_form-h')[0].setAttribute('rObjectState', '');
                $('#inputName').val('');
                $('#inputAddress').val('');
                $('#inputPrice').val('');
                $('#inputDescription').val('');

                // скрытие модального окна
                $('#addEditRObject').modal('hide');

                // загрузка обновленного списка объектов
                getRecords();
            },
            'text'
        );
    });

    // сортировка по возрастанию цены
    $('#sortPriceAsc').click(function () {

        getRecords('asc');
    });

    // сортировка по убыванию цены
    $('#sortPriceDesc').click(function () {

        getRecords('desc');
    });

    //
    $('#search').click(function () {

        getRecords('', $('#buidingNumber').val());
        $('#buidingNumber').val('')
    });

    // запуск получения списка объектов
    getRecords();

    // проверка на вход в систему
    checkLogin();
})